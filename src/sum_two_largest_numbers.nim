import types

##[
Finds the two largest numbers in a sequence and returns their sum.

Syntax
******
Summing
######################
.. code-block::
  sum_two_largest_numbers(@[1, 2, 3, 4, 5])
  # => 9
]##

func sum_two_largest_numbers*(numbers: AtLeastTwo[int]): int =
  var two_largest = [low(int), low(int)]

  for n in numbers:
    if n > two_largest[0]:
      two_largest = [n, two_largest[0]]
    elif n > two_largest[1]:
      two_largest = [two_largest[0], n]

  two_largest[0] + two_largest[1]
