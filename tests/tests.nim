import unittest
import strformat
import sum_two_largest_numbers

let small = (input: [1, 1], output: 2)
let negative_and_positive = (input: [0, -5, 5, 2, 5], output: 10)
let negative = (input: [-10, -5, -1, -3], output: -2)

test fmt"Sums small sequences: {small}":
  check(sum_two_largest_numbers(small.input) == small.output)

test fmt"Sums negative and positive numbers properly: {negative_and_positive}":
  check(sum_two_largest_numbers(negative_and_positive.input) ==
      negative_and_positive.output)

test fmt"Sums negative numbers properly: {negative}":
  check(sum_two_largest_numbers(negative.input) == negative.output)
