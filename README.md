# Sum the Largest 2 Numbers in a Sequence

This function accepts any sequence of numbers and sums the highest 2 values.

## Usage

```nim
import sum_two_largest_numbers

echo sum_two_largest_numbers(@[5, 3, 10, 8]);

>> 18
```

## Developing & Testing

If you want to clone this and work on it, since the package is a single file and function I find the easiest way to work is to make changes and run the tests.

To run the tests, run `nimble test`.
