# Package

version       = "0.1.0"
author        = "Steve Adams"
description   = "Sum the two largest numbers in a sequence of numbers"
license       = "MIT"

srcDir   = "src"
skipDirs = @["tests"]


# Dependencies

requires "nim >= 1.0.4"

task docs, "Docs":
  exec "nim doc -o:./docs/index.html ./src/sum_two_largest_numbers.nim"
